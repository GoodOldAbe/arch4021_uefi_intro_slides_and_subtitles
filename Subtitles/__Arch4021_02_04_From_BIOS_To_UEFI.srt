1
00:00:00,080 --> 00:00:06,799
in 2006 UEFI version 2.0

2
00:00:03,120 --> 00:00:09,519
brought us cryptography and security.

3
00:00:06,799 --> 00:00:11,200
We also get new new

4
00:00:09,519 --> 00:00:14,080
specification called, Platform

5
00:00:11,200 --> 00:00:15,920
Initialization, specification in version

6
00:00:14,080 --> 00:00:17,680
1.0. I will explain what's the difference

7
00:00:15,920 --> 00:00:19,520
between two and like what's

8
00:00:17,680 --> 00:00:23,119
included in those

9
00:00:19,520 --> 00:00:24,480
then in 2007 we have 2.1

10
00:00:23,119 --> 00:00:26,320
where we're getting network

11
00:00:24,480 --> 00:00:31,039
authentications. So,

12
00:00:26,320 --> 00:00:33,760
https and other secure protocols

13
00:00:31,039 --> 00:00:36,640
for UEFI firmware for

14
00:00:33,760 --> 00:00:38,800
unified extensible firmware interface

15
00:00:36,640 --> 00:00:40,640
there is also architecture of user

16
00:00:38,800 --> 00:00:41,680
interface. So

17
00:00:40,640 --> 00:00:42,960
all these

18
00:00:41,680 --> 00:00:44,960
grey blue

19
00:00:42,960 --> 00:00:48,000
screens had to be somehow

20
00:00:44,960 --> 00:00:50,559
provided to the UEFI environment and

21
00:00:48,000 --> 00:00:53,120
maybe something even more sophisticated.

22
00:00:50,559 --> 00:00:55,600
Then in 2010

23
00:00:53,120 --> 00:00:57,520
UEFI expands even more

24
00:00:55,600 --> 00:00:58,480
but

25
00:00:57,520 --> 00:01:01,039

26
00:00:58,480 --> 00:01:04,239
the UEFI started to

27
00:01:01,039 --> 00:01:06,000
be deployed on multiple modern hardware

28
00:01:04,239 --> 00:01:10,080
at that time and

29
00:01:06,000 --> 00:01:13,119
BIOS started to decline. And

30
00:01:10,080 --> 00:01:16,159
many UEFI implementation had to

31
00:01:13,119 --> 00:01:18,880
provide backward compatibility in

32
00:01:16,159 --> 00:01:21,360
form of legacy compatibility modules,

33
00:01:18,880 --> 00:01:23,439
the custom mode and so on. So,

34
00:01:21,360 --> 00:01:25,280
probably most of you probably remember

35
00:01:23,439 --> 00:01:27,600
that we can go to

36
00:01:25,280 --> 00:01:30,079
BIOS menu and switch from UEFI mode to

37
00:01:27,600 --> 00:01:32,000
legacy mode and in that way we can boot

38
00:01:30,079 --> 00:01:34,320
some more legacy

39
00:01:32,000 --> 00:01:36,560
software. In 2018 there was interesting

40
00:01:34,320 --> 00:01:39,840
even happened because

41
00:01:36,560 --> 00:01:43,280
Microsoft published a Project Mu

42
00:01:39,840 --> 00:01:46,079
and Project Mu is a fork

43
00:01:43,280 --> 00:01:48,960
of EDKII.

44
00:01:46,079 --> 00:01:50,560
Which is which contains support for

45
00:01:48,960 --> 00:01:51,680
Surface

46
00:01:50,560 --> 00:01:53,119
Pro

47
00:01:51,680 --> 00:01:54,720
notebooks

48
00:01:53,119 --> 00:01:56,960
and Hyper-V.

49
00:01:54,720 --> 00:02:00,479
Also ARM announces server ready

50
00:01:56,960 --> 00:02:03,840
certification program which requires

51
00:02:00,479 --> 00:02:06,479
UEFI, ACPI as an BIOS specification

52
00:02:03,840 --> 00:02:09,920
which means that ARM recognized

53
00:02:06,479 --> 00:02:11,360
UEFI environment as a default for server

54
00:02:09,920 --> 00:02:14,000
environment.

55
00:02:11,360 --> 00:02:16,560


56
00:02:14,000 --> 00:02:19,520
So, from that we can say that BIOS served its

57
00:02:16,560 --> 00:02:20,480
purpose very well for a long time.

58
00:02:19,520 --> 00:02:23,920


59
00:02:20,480 --> 00:02:25,760
In the beginning even user application

60
00:02:23,920 --> 00:02:27,680
there were even user applications many

61
00:02:25,760 --> 00:02:29,680
user applications using

62
00:02:27,680 --> 00:02:31,040
its runtime services.

63
00:02:29,680 --> 00:02:33,040
But

64
00:02:31,040 --> 00:02:36,400
technology change,

65
00:02:33,040 --> 00:02:40,239
force it to remove BIOS from the

66
00:02:36,400 --> 00:02:45,200
landscape and uh eventually will

67
00:02:40,239 --> 00:02:48,319
ending in 2020 and right now 2022 but

68
00:02:45,200 --> 00:02:51,200
the things continues. In 2020

69
00:02:48,319 --> 00:02:53,920
Intel decides that they remove support

70
00:02:51,200 --> 00:02:55,519
for legacy BIOS for new platforms and

71
00:02:53,920 --> 00:02:58,480
there will be only

72
00:02:55,519 --> 00:03:02,159
UEFI which is validated and

73
00:02:58,480 --> 00:03:04,959
delivered by Intel and

74
00:03:02,159 --> 00:03:06,879
supported by Intel.

75
00:03:04,959 --> 00:03:08,560
Some vendors definitely still will

76
00:03:06,879 --> 00:03:10,640
provide for some time compatibility

77
00:03:08,560 --> 00:03:13,680
support module which is because it is

78
00:03:10,640 --> 00:03:16,560
very necessary for some application

79
00:03:13,680 --> 00:03:19,680
but over time we will probably switch to

80
00:03:16,560 --> 00:03:21,519
UEFI interface and there is not much that

81
00:03:19,680 --> 00:03:24,319
can compete with that.

82
00:03:21,519 --> 00:03:26,720
There's ongoing effort in open source

83
00:03:24,319 --> 00:03:28,720
firmware community to mitigate problems

84
00:03:26,720 --> 00:03:30,080
related to UEFI and I will talk about

85
00:03:28,720 --> 00:03:33,120
that in

86
00:03:30,080 --> 00:03:34,799
UEFI criticism section.

87
00:03:33,120 --> 00:03:37,760


88
00:03:34,799 --> 00:03:40,959
And the last UEFI specification version

89
00:03:37,760 --> 00:03:45,280
2.9 was published in march 2021 and we

90
00:03:40,959 --> 00:03:45,280
will base this lecture on that version.



