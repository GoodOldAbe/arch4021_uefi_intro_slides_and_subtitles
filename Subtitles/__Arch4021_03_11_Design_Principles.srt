1
00:00:00,480 --> 00:00:04,720
Okay, so let's discuss

2
00:00:02,639 --> 00:00:07,600
design principles.

3
00:00:04,720 --> 00:00:09,920
So, main design principles for UEFI

4
00:00:07,600 --> 00:00:11,679
specification are reuse of existing table

5
00:00:09,920 --> 00:00:13,840
based interfaces,

6
00:00:11,679 --> 00:00:17,279
so we have bunch of various

7
00:00:13,840 --> 00:00:19,680
table interfaces 

8
00:00:17,279 --> 00:00:22,880
right now in BIOS but

9
00:00:19,680 --> 00:00:25,039
even in very early EFI development stage

10
00:00:22,880 --> 00:00:28,080
there were also

11
00:00:25,039 --> 00:00:29,679
some of those tables. So, for example ACPI

12
00:00:28,080 --> 00:00:31,760
which is responsible for power

13
00:00:29,679 --> 00:00:34,559
management and more and more features

14
00:00:31,760 --> 00:00:38,079
right now, SMBIOS which is

15
00:00:34,559 --> 00:00:39,280
responsible for providing or DMI 

16
00:00:38,079 --> 00:00:42,079
recently

17
00:00:39,280 --> 00:00:44,239
we're calling it DMI which is 

18
00:00:42,079 --> 00:00:46,559
responsible for providing some

19
00:00:44,239 --> 00:00:48,399
information

20
00:00:46,559 --> 00:00:50,879
about components in

21
00:00:48,399 --> 00:00:54,239
the system, multiprocessor table,

22
00:00:50,879 --> 00:00:56,000
device tree for some platforms.

23
00:00:54,239 --> 00:00:58,399
There are also various network related

24
00:00:56,000 --> 00:00:59,680
tables which may be needed for network

25
00:00:58,399 --> 00:01:03,120
boot or

26
00:00:59,680 --> 00:01:06,159
network capabilities inside the firmware,

27
00:01:03,120 --> 00:01:08,080
and there are also new tables like

28
00:01:06,159 --> 00:01:09,680
memory attributes table

29
00:01:08,080 --> 00:01:11,600


30
00:01:09,680 --> 00:01:14,400
which standardize

31
00:01:11,600 --> 00:01:15,759
ways of passing information about

32
00:01:14,400 --> 00:01:18,799
memory table

33
00:01:15,759 --> 00:01:20,960
or other things between firmware and

34
00:01:18,799 --> 00:01:23,439
operating system.

35
00:01:20,960 --> 00:01:25,680
There is also system partition

36
00:01:23,439 --> 00:01:27,200
which we discussed in previously and

37
00:01:25,680 --> 00:01:28,479
this is also one of the design

38
00:01:27,200 --> 00:01:30,720
principles,

39
00:01:28,479 --> 00:01:32,799
it is storage for software consumer

40
00:01:30,720 --> 00:01:36,400
during the firmware OS transition so

41
00:01:32,799 --> 00:01:39,280
bootloader, kernel, initramfs, and maybe

42
00:01:36,400 --> 00:01:41,600
hypervisor if someone using that.

43
00:01:39,280 --> 00:01:43,920
There are also boot services

44
00:01:41,600 --> 00:01:45,920
and which give access to platform

45
00:01:43,920 --> 00:01:48,960
hardware resources for OS

46
00:01:45,920 --> 00:01:51,520
loader and in that way abstract

47
00:01:48,960 --> 00:01:53,840
access to hardware, and runtime services

48
00:01:51,520 --> 00:01:56,880
which give access to platform hardware

49
00:01:53,840 --> 00:01:59,759
resources for operating system.

50
00:01:56,880 --> 00:02:03,920
UEFI design principles

51
00:01:59,759 --> 00:02:06,320
define also a couple information around

52
00:02:03,920 --> 00:02:09,360
hierarchical structure of the

53
00:02:06,320 --> 00:02:11,520
firmware in itself and how it is stored

54
00:02:09,360 --> 00:02:13,680
inside firmware

55
00:02:11,520 --> 00:02:15,440
storage.

56
00:02:13,680 --> 00:02:16,959
So, the higher level

57
00:02:15,440 --> 00:02:19,280


58
00:02:16,959 --> 00:02:20,800
hierarchy is firmware device which

59
00:02:19,280 --> 00:02:23,920
represents the whole

60
00:02:20,800 --> 00:02:27,040
device in which firmware can be stored,

61
00:02:23,920 --> 00:02:28,239
this is typically SPI NOR flash.

62
00:02:27,040 --> 00:02:30,720


63
00:02:28,239 --> 00:02:33,760
The firmware volumes which are logical

64
00:02:30,720 --> 00:02:36,319
representation of a firmware device

65
00:02:33,760 --> 00:02:38,800
visible from the perspective of the

66
00:02:36,319 --> 00:02:41,200
executing firmware,

67
00:02:38,800 --> 00:02:44,400
and a full firmware image typically

68
00:02:41,200 --> 00:02:46,959
consists of at least one firmware volume

69
00:02:44,400 --> 00:02:51,360
but it can have multiple one

70
00:02:46,959 --> 00:02:53,599
and firmware volumes consist of GUIDs,

71
00:02:51,360 --> 00:02:57,440
GUIDs identified files

72
00:02:53,599 --> 00:02:58,720
and those files consist of sections

73
00:02:57,440 --> 00:03:02,000
for example

74
00:02:58,720 --> 00:03:03,440
code section data section

75
00:03:02,000 --> 00:03:04,640
and

76
00:03:03,440 --> 00:03:08,319
code section

77
00:03:04,640 --> 00:03:10,400
contain our portable executable

78
00:03:08,319 --> 00:03:13,120
file created by

79
00:03:10,400 --> 00:03:15,120
a module compilation.

80
00:03:13,120 --> 00:03:17,519
There are various files recognized by

81
00:03:15,120 --> 00:03:20,480
UEFI specification

82
00:03:17,519 --> 00:03:22,800
but that's the most important

83
00:03:20,480 --> 00:03:26,720
hierarchical components

84
00:03:22,800 --> 00:03:26,720
that the UEFI specification define.

85
00:03:27,280 --> 00:03:30,799
When passing

86
00:03:28,799 --> 00:03:34,239
information from

87
00:03:30,799 --> 00:03:35,440
PI to further phases,

88
00:03:34,239 --> 00:03:38,319


89
00:03:35,440 --> 00:03:40,560
there are kinds of blocks used for that

90
00:03:38,319 --> 00:03:42,080
and those kinds of blocks are binary

91
00:03:40,560 --> 00:03:45,360
data structures

92
00:03:42,080 --> 00:03:47,200
which are stacked together so that

93
00:03:45,360 --> 00:03:49,760
amount of possible information can be

94
00:03:47,200 --> 00:03:51,599
flexible so it always

95
00:03:49,760 --> 00:03:53,680
we can add another

96
00:03:51,599 --> 00:03:56,239
Hand-Off-Block (HOB),

97
00:03:53,680 --> 00:03:59,360
and this kind of block list is built in

98
00:03:56,239 --> 00:04:00,640
PEI phase where there is limited

99
00:03:59,360 --> 00:04:03,200
memory.

100
00:04:00,640 --> 00:04:03,200
And then

101
00:04:03,439 --> 00:04:06,879
those

102
00:04:04,319 --> 00:04:09,680
information

103
00:04:06,879 --> 00:04:13,200
which are gathered in hand-off blocks in

104
00:04:09,680 --> 00:04:15,599
PEI and potentially SEC phase those are

105
00:04:13,200 --> 00:04:19,199
very early phases of boot process are

106
00:04:15,599 --> 00:04:22,000
passed to the further phase called DXE

107
00:04:19,199 --> 00:04:25,919
which is our main initialization

108
00:04:22,000 --> 00:04:29,280
phase defined in UEFI spec.

109
00:04:25,919 --> 00:04:31,120
And HOBs can contain information

110
00:04:29,280 --> 00:04:32,960
related to

111
00:04:31,120 --> 00:04:35,199
I/O

112
00:04:32,960 --> 00:04:37,680
to memory

113
00:04:35,199 --> 00:04:39,680
to resources allocation

114
00:04:37,680 --> 00:04:43,360
to some firmware devices which were

115
00:04:39,680 --> 00:04:45,600
detected and how those are organized

116
00:04:43,360 --> 00:04:47,600
this is what

117
00:04:45,600 --> 00:04:50,479
further phases use.

118
00:04:47,600 --> 00:04:52,479
HOBs also can contain information

119
00:04:50,479 --> 00:04:55,199
about boot path

120
00:04:52,479 --> 00:04:57,680
and and various other not 

121
00:04:55,199 --> 00:04:59,520
defined information because HOBs can be

122
00:04:57,680 --> 00:05:02,240
customized for the

123
00:04:59,520 --> 00:05:04,720
OEM needs.

124
00:05:02,240 --> 00:05:06,880
So, HOB format is not

125
00:05:04,720 --> 00:05:09,840
exactly regulated

126
00:05:06,880 --> 00:05:12,160
in any specification there are just HOBs

127
00:05:09,840 --> 00:05:13,919
HOB headers and

128
00:05:12,160 --> 00:05:15,280
and some GUIDs

129
00:05:13,919 --> 00:05:17,280
and that's it.

130
00:05:15,280 --> 00:05:21,360
The data which is inside has no

131
00:05:17,280 --> 00:05:24,000
format and is used as a free form.

132
00:05:21,360 --> 00:05:27,360
The consumer of the HOB have to

133
00:05:24,000 --> 00:05:30,400
know the structure of what is

134
00:05:27,360 --> 00:05:32,800
received from previous phase and there

135
00:05:30,400 --> 00:05:35,840
can be some exceptions

136
00:05:32,800 --> 00:05:38,479
for this free form like

137
00:05:35,840 --> 00:05:41,199
some ACPI tables

138
00:05:38,479 --> 00:05:43,680
are passed in HOBs as well as SMBIOS

139
00:05:41,199 --> 00:05:44,960
tables are passed in HOBs, so those are

140
00:05:43,680 --> 00:05:49,880
definitely

141
00:05:44,960 --> 00:05:49,880
compliant to the relevant specification.




