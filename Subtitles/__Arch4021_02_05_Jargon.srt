1
00:00:00,840 --> 00:00:05,339
Let's talk a little bit about Jargon,

2
00:00:02,760 --> 00:00:10,800
correctness of the given naming scheme so

3
00:00:05,339 --> 00:00:13,259
using terms BIOS, UEFI, EFI, UEFI, BIOS is

4
00:00:10,800 --> 00:00:14,780
subject to some debate even some small

5
00:00:13,259 --> 00:00:17,880
controversy.

6
00:00:14,780 --> 00:00:20,520
So, what we have to know is that all

7
00:00:17,880 --> 00:00:23,880
major industry players from the computer

8
00:00:20,520 --> 00:00:27,960
system supply chain typically use BIOS

9
00:00:23,880 --> 00:00:30,320
name as a name for every bootstrap

10
00:00:27,960 --> 00:00:35,059
firmware for computer systems.

11
00:00:30,320 --> 00:00:41,100
Microsoft seemed to promote term UEFI

12
00:00:35,059 --> 00:00:43,559
Apple mostly uses the term EFI but in

13
00:00:41,100 --> 00:00:46,440
security documentation they make sure

14
00:00:43,559 --> 00:00:49,260
that the term is already UEFI which is

15
00:00:46,440 --> 00:00:52,800
like a modern correct name of the

16
00:00:49,260 --> 00:00:55,739
specification and we can also agree that

17
00:00:52,800 --> 00:00:58,879
typically firmware that provides

18
00:00:55,739 --> 00:01:02,940
functionality of old BIOS so

19
00:00:58,879 --> 00:01:05,880
interrupts some legacy compatibility is

20
00:01:02,940 --> 00:01:08,460
called legacy BIOS but platform

21
00:01:05,880 --> 00:01:10,860
initialization and unified XM symbol

22
00:01:08,460 --> 00:01:14,220
firmware interface specification

23
00:01:10,860 --> 00:01:18,780
compatible firmware implementations are

24
00:01:14,220 --> 00:01:22,140
sometimes called UEFI BIOS or just UEFI. So

25
00:01:18,780 --> 00:01:26,100
can we really say that UEFI

26
00:01:22,140 --> 00:01:29,400
replaced BIOS? It seems that no but we

27
00:01:26,100 --> 00:01:32,460
can say that UEFI and PI compliant

28
00:01:29,400 --> 00:01:35,100
firmware implementations replaced

29
00:01:32,460 --> 00:01:37,439
previous BIOS implementations.

30
00:01:35,100 --> 00:01:40,320
So, during this presentation just to

31
00:01:37,439 --> 00:01:43,400
avoid this confusion any term like

32
00:01:40,320 --> 00:01:46,500
UEFI, BIOS and EFI BIOS are used

33
00:01:43,400 --> 00:01:49,079
interchangeably. Also please note

34
00:01:46,500 --> 00:01:52,200
that if you're looking at UEFI and PI

35
00:01:49,079 --> 00:01:54,899
specification you can still find EFI

36
00:01:52,200 --> 00:01:58,200
acronym especially in the diagrams in

37
00:01:54,899 --> 00:02:00,840
the graphics, but of course that those

38
00:01:58,200 --> 00:02:05,720
are just not updated one and you have to

39
00:02:00,840 --> 00:02:05,720
assume they mean UEFI in this case.


