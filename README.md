# Architecture 4021: UEFI Introduction

This repository contain files to generate PDF presentations of [Open Security
Training2](https://ost2.fyi) course [Architecture 4021: Introductory UEFI](https://ost2.fyi/Arch4021)

## Building

### Linux

Requirements:
* Python 3

```shell
git clone git@gitlab.com:opensecuritytraining/arch4021_uefi_intro_slides_and_subtitles.git
cd arch4021_uefi_intro_slides_and_subtitles
git submodule update --init --checkout
python3 -m http.server
```

Now you can open web browser to access `0.0.0.0:8000`, for example:

```shell
chromium 0.0.0.0:8000
```

![](images/presentations_list.png)

Open file with `.html` extension. That should render presentation for given
module.

![](images/rendered_presentation.png)

### Printing PDFs in Chromium

Click `Ctrl+P` and save as PDF.

![](images/chrome_printing_screen.png)

### Printing PDFs using command line

Automated PDF generation without opening web browser:

```
./remark-templates/scripts/print-pdfs.sh .
```

## How to contribute

After modification make sure, using Building procedure, that presentation in
web browser as well as in PDFs render correctly.

Before sending merge request make sure TBD/TODO and pre-commit checks pass.

### Make sure no TBD or TODO content is displayed

Find all occurrences:

```shell
grep -E "TBD|TODO" --include="*.md" . -r
```

Iterate over all occurrences and check if TBD or TODO is not visible on the
presentation. It is ok to have TBD/TODO in speaker notes section (below `???`
marker).

### pre-commit hooks

* [Install pre-commit](https://pre-commit.com/index.html#install), if you
  followed [local build](#local-build) procedure `pre-commit` should be
  installed
* [Install go](https://go.dev/doc/install)

#### Installation in Debian-based Linux

```bash
virtualenv -p $(which python3) venv
source venv/bin/activate
pip install -r requirements.txt
```

* Install hooks into repo:

```bash
pre-commit install --hook-type commit-msg
```

* Enjoy automatic checks on each `git commit` action!

### Run hooks on all files

For example, when adding new hooks or configuring existing ones:

```bash
pre-commit run --all-files
```

### To skip verification

In some cases, it may be needed to skip `pre-commit` tests. To do that, please
use:

```bash
git commit --no-verify
```
